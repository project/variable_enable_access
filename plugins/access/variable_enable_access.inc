<?php

/**
 * @file
 * An access plugin based on variables of type "enable".
 */

$plugin = array(
  'title' => t('Variable is enabled'),
  'description' => t('Checks if a variable is enabled'),
  'callback' => 'variable_enable_access_variable_enabled',
  'summary' => 'variable_enable_access_variable_enabled_summary',
  'settings form' => 'variable_enable_access_variable_enabled_form',
  'settings form submit' => 'variable_enable_access_variable_enabled_form_submit',
);

/**
 * Provide a summary including which variable is checked.
 */
function variable_enable_access_variable_enabled_summary($conf, $context) {
  $variables = variable_enable_access_get_enable_variables();
  return t('Check if "!variable" is enabled', array('!variable' => $variables[$conf['variable']]['title']));
}

/**
 * Configuration form for the access plugin. Choose variable.
 */
function variable_enable_access_variable_enabled_form($form, &$form_state) {
  $form['settings']['variable'] = array(
    '#type' => 'select',
    '#title' => t('Variable'),
    '#options' => variable_enable_access_get_enable_variables_options(),
  );

  return $form;
}

/**
 * Submit handler for the settings form.
 */
function variable_enable_access_variable_enabled_form_submit($form, &$form_state) {
  // Copy everything from our defaults.
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Controls access based on whether the variable is enabled or not.
 */
function variable_enable_access_variable_enabled($conf, $context) {
  if (!empty($conf['variable']) && variable_get_value($conf['variable'])) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}
